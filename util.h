struct Point {
  double x;
  double y;

  double distance(const Point& other) {
    return (x - other.x) * (x - other.x)
      + (y - other.y) * (y - other.y);
  }

  void pup(PUP::er &p) {
    p | x;
    p | y;
  }
};
